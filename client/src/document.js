/**
 * Created by danielharis on 14/03/2016.
 */

Template.document.helpers({

    'isActiveProject': function() {
        return (Projects.findOne({ active: true })) ? true : false;
    },
    'activeProjectDocument': function() {
        var activeProject = Projects.findOne({ active: true });
        return activeProject ? activeProject.document : "No Document";
    }

});

Template.document.events({

    /**
     * Upload a specification document
     */
    'change #chooseDoc': function(evt) {
        var reader = new FileReader();
        reader.onload = function() {
            Meteor.call('updateProject', { active: true }, { $set: { document: reader.result } });
        };
        reader.readAsText(evt.target.files[0]);
    },

    /**
     * Highlight selected requirement and assign it to active test case
     */
    'mouseup #doc': function () {
        var activeProject = Projects.findOne({ active: true }),
            userSelection = window.getSelection().getRangeAt(0),
            selectionText = window.getSelection().toString();
        // if project already templated, set templated flag to false
        if (activeProject.templated)
            Meteor.call('updateProject', { _id: activeProject._id }, { $set: { templated: false } });
        // highlight only selection that is longer than 0 characters
        if (userSelection.endOffset - userSelection.startOffset > 0) {
            var safeRanges = getSafeRanges(userSelection);
            console.log(safeRanges);
            for (var i = 0; i < safeRanges.length; i++)
                highlightRange(safeRanges[i]);
            // if none of the tests are selected, have to store selected lines
            if (!Session.get('selectedTest')) {
                var a = Session.get('selectedLines');
                a.push({
                    lineId: Session.get('selectCounter'),
                    content: String(selectionText)
                });
                Session.set('selectedLines', a);
                //if there is test selected
            } else {
                Meteor.call('updateTest', { _id: Session.get('selectedTest') },
                    { $push: { lines: {
                        lineId: Session.get('selectCounter'),
                        content: selectionText
                    }}}
                );
            }
            // update document content
            var doc = $('#doc').html();
            $('#doc').empty();
            // update document in DB
            Meteor.call('updateProject', { active: true }, { $set: { document: doc } });
        }
    },

    /**
     * Remove highlighted requirement
     */
    'click span': function (e) {
        var activeProject = Projects.findOne({ active: true });
        // if project already templated, set templated flag to false
        if (activeProject.templated)
            Meteor.call('updateProject', { _id: activeProject._id }, { $set: { templated: false } });
        // first we have to remove line from DB
        var currentLineId = e.target.getAttribute("lineId");
        // remove selected requirement
        Meteor.call('updateTest', { _id: Session.get('selectedTest') }, { $pull: { lines: { lineId: currentLineId }}});
        // finally remove highlight
        $(e.target).contents().unwrap();
        // update document content
        var doc = $('#doc');
        doc.each(function (index, text) { text.normalize() });
        Meteor.call('updateProject', { active: true }, { $set: { document: doc.html() } });
    },

    /**
     * Zoom in and Zoom out the specification document
     */
    'click #plusBtn': function() {
        var obj = document.getElementById('docContainer'),
            currentSize = parseInt(obj.style.fontSize);
        obj.style.fontSize = (currentSize + 1) + "px";
    },
    'click #minusBtn': function() {
        var obj = document.getElementById('docContainer'),
            currentSize = parseInt(obj.style.fontSize);
        obj.style.fontSize = (currentSize - 1) + "px";
    }

});

/**
 * Function highlights given range in document
 */
function highlightRange(range) {
    // increment counter of selection
    Session.set('selectCounter', Random.id(10));
    var newNode = document.createElement("span");
    newNode.setAttribute("class", "docLine");
    newNode.setAttribute("lineId", Session.get('selectCounter'));
    // set color only if a test is selected
    if (Session.get('selectedTest')) {
        var testColor = TestCases.findOne({ _id: Session.get('selectedTest') }).color;
        newNode.setAttribute("style", "background-color: " + testColor + ";color:ghostwhite;");
    }
    range.surroundContents(newNode);
}

/**
 * Function returns array of "safe" ranges in document
 */
function getSafeRanges(dangerous) {
    var a = dangerous.commonAncestorContainer,
    // Starts -- Work inward from the start, selecting the largest safe range
        s = [],
        rs = [];
    if (dangerous.startContainer != a)
        for (var i = dangerous.startContainer; i != a; i = i.parentNode)
            s.push(i);
    if (0 < s.length) {
        for (var i = 0; i < s.length; i++) {
            var xs = document.createRange();
            if (i) {
                xs.setStartAfter(s[i-1]);
                xs.setEndAfter(s[i].lastChild);
            } else {
                xs.setStart(s[i], dangerous.startOffset);
                xs.setEndAfter((s[i].nodeType == Node.TEXT_NODE) ? s[i] : s[i].lastChild);
            }
            rs.push(xs);
        }
    }
    // Ends -- basically the same code reversed
    var e = [], re = [];
    if (dangerous.endContainer != a) {
        for (var i = dangerous.endContainer; i != a; i = i.parentNode) {
            e.push(i);
        }
    }
    if (0 < e.length) {
        for (var i = 0; i < e.length; i++) {
            var xe = document.createRange();
            if (i) {
                xe.setStartBefore(e[i].firstChild);
                xe.setEndBefore(e[i - 1]);
            } else {
                xe.setStartBefore((e[i].nodeType == Node.TEXT_NODE) ? e[i] : e[i].firstChild);
                xe.setEnd(e[i], dangerous.endOffset);
            }
            re.unshift(xe);
        }
    }
    // Middle -- the uncaptured middle
    if ((0 < s.length) && (0 < e.length)) {
        var xm = document.createRange();
        xm.setStartAfter(s[s.length - 1]);
        xm.setEndBefore(e[e.length - 1]);
    } else {
        return [dangerous];
    }
    // Concat
    rs.push(xm);
    return rs.concat(re);
}
