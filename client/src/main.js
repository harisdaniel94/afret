/**
 * Created by danielharis on 14/03/2016.
 */

/**
 * Meteor app structure
 * http://stackoverflow.com/questions/10122977/what-are-the-best-practices-for-structuring-a-large-meteor-app-with-many-html-te?rq=1
 *
 * Random HEX color generator
 * http://stackoverflow.com/questions/5092808/how-do-i-randomly-generate-html-hex-color-codes-using-javascript
 * */

/**
 * Session variables
 */
Session.setDefault('selectedTest', '');
Session.setDefault('previousTest', '');
Session.setDefault('selectCounter', '');
Session.setDefault('selectedLines', []);
Session.setDefault('savedProjects', []);

/**
 * Gain access to all projects and test-cases
 */
Meteor.subscribe('allProjects');
Meteor.subscribe('allTests');

/**
 * Template helper functions
 * - able to preprocess handlebar parameters
 * - 1 {{ inc @index }}
 * - 2 {{ formatDate createdAt }}
 */
Template.registerHelper('inc', function (index) {
    return index + 1;
});
Template.registerHelper('formatDate', function (date) {
    return date.toLocaleString();
});

/**
 * Test deselect functionality
 * No deselect only if clicked on a test or on the document
 */
$('body').click(function(evt) {
    // if clicked outside of test-cases or document, deselect test-case
    if (!evt.target.closest('#doc') &&
        !evt.target.closest('tbody#tc-table-body') &&
        Session.get('selectedTest')) {
        // prevent showing previous test's lines
        var currTest = TestCases.findOne({ _id: Session.get('selectedTest') });
        if (currTest && currTest.lines.length)
            for (var i = 0; i < currTest.lines.length; i++)
                $('span[lineId=' + currTest.lines[i].lineId + ']').removeAttr("style");
        // deselect active test
        Session.set('selectedTest', '');
    }
});

/**
 * Customize toaster appearance and behavior
 */
toastr.options.showMethod = 'slideDown';
toastr.options.hideMethod = 'slideUp';
toastr.options.closeMethod = 'slideUp';
toastr.options.preventDuplicates = true;
