/**
 * Created by danielharis on 14/03/2016.
 */

Template.navigation.helpers({
    'activeProject': function() {
        var activeProject = Projects.findOne({ active: true});
        return (activeProject) ? activeProject : false;
    },
    'otherProjects': function() {
        return Projects.find({ active: false });
    },
    'enableReportBtn': function() {
        var activeProject = Projects.findOne({ active: true, templated: true });
        return activeProject && TestCases.find({ projectId: activeProject._id, executed: true }).count();
    }
});

Template.navigation.events({

    /**
     * Create a new project
     */
    'submit #createProjectForm': function(evt) {
        evt.preventDefault();
        var name = $('#projectName').val(),
            lang = $('#projectLang').val(),
            agentUrl = $('#agentURL').val();
        if (!name || !lang || !agentUrl) {
            toastr.warning("Missing values");
            return;
        }
        // validate new husband
        if (Projects.findOne({ name: name })) {
            toastr.warning("Project with name '" + name + "' already exists", "Project name duplicate");
            return;
        } else if (!name.match(/^[a-zA-Z_][a-zA-Z0-9_]*$/)) {
            toastr.warning("'" + name + "' doesn't match '^[a-zA-Z_][a-zA-Z0-9_]*$'", "Invalid project name");
            return;
        }
        // make previous project inactive
        Meteor.call('updateProject', { active: true }, { $set: { active: false } });
        // insert new project into DB
        Projects.insert({
            name: name,
            language: lang,
            agentUrl: agentUrl,
            document: "No document uploaded",
            templated: false
        });
        // hide modal window
        $('#createProjectModal').modal('hide');
        // send request to create project structure on agent
        var createProject = $.ajax({
            url: agentUrl + 'create',
            data: { projectName: name },
            crossDomain: true,
            contentType: 'application/json',
            timeout: 3000
        });
        // successful request
        createProject.done(function(resp) {
            if (resp.created) {
                toastr.success(name + " created!");
            } else {
                toastr.info(name + " already exists!");
            }
        });
        // request failed
        createProject.fail(function(res, status, err) {
            if (res.status === 0)
                toastr.error("Check if agent runs", "Error sending request");
            else
                toastr.error(err.message);
        });
    },

    /**
     * Removes a project from DB
     */
    'click #deleteProjectBtn': function() {

        var activeProject = Projects.findOne({ active: true }),
            deleteModal = $('#deleteProjectModal');
        deleteModal.modal('show');
        // local delete, removes project only from server DB, not from Agent
        $('#localDeleteBtn').click(function() {
            console.log("clicked local delete btn");
            // remove current project and all of its tests
            Meteor.call('removeProject', { _id: activeProject._id });
            Meteor.call('removeTest', { projectId: activeProject._id });
        });
        // global delete, project is removed everywhere
        $('#globalDeleteBtn').click(function() {
            // remove current project and all of its tests
            Meteor.call('removeProject', { _id: activeProject._id });
            Meteor.call('removeTest', { projectId: activeProject._id });
            var deleteProject = $.ajax({
                url: activeProject.agentUrl + 'delete',
                data: { projectName: activeProject.name },
                crossDomain: true,
                contentType: 'application/json',
                timeout: 3000
            });
            deleteProject.done(function(resp) {
                if (resp.deleted)
                    toastr.success(activeProject.name + " deleted!");
                else
                    toastr.info(activeProject.name + " not existed on server!");
            });
            deleteProject.fail(function(res, status, err) {
                if (res.status === 0)
                    toastr.error("Check if agent runs", "Error sending request");
                else
                    toastr.error(err.message);
            });
        });

    },

    /**
     * Project import
     */
    'click #importProjectBtn': function() {

        // construct an input element which will trigger file select
        var input = $(document.createElement('input'));
        input.attr('type', 'file');
        input.attr('accept', 'application/json');
        input.trigger('click').on('change', function(evt) {
            var reader = new FileReader();
            reader.onload = function() {
                var importObj = JSON.parse(reader.result);
                // make previous project inactive
                Meteor.call('updateProject', { active: true }, { $set: { active: false } });
                Meteor.call('importProject', importObj, function(err, result) {
                    if (!result) {
                        toastr.warning("File does not match DB schema", "Invalid export file");
                        return;
                    }
                });
                var agentUrl = Projects.findOne({ active: true }).agentUrl;
                var createProject = $.ajax({
                    url: agentUrl + 'create',
                    data: { projectName: importObj.project.name },
                    crossDomain: true,
                    contentType: 'application/json',
                    timeout: 3000
                });
                createProject.done(function() {
                    toastr.success(importObj.project.name + " imported!");
                });
                createProject.fail(function(res, status, err) {
                    if (res.status === 0)
                        toastr.error("Check if agent runs", "Error sending request");
                    else
                        toastr.error(err.message);
                });
            };
            reader.readAsText(evt.target.files[0]);
        });

    },

    /**
     * Project exports into a file on downloads
     */
    'click #exportProjectBtn': function() {
        // update current project's document
        var activeProject = Projects.findOne({ active: true });
        var projectTests = TestCases.find({ projectId: activeProject._id });
        // collect project's data
        var exportData = collectProjectData({
            project: activeProject,
            tests: projectTests
        });
        var exportJsonData = JSON.stringify(exportData),
            blob = new Blob([exportJsonData], { type: "application/json" });
        // save export data as
        saveAs(blob, activeProject.name + '.' + getExportDate() + ".export.json");
    },

    /**
     * Save functionality of AFRET, project is then located on agent's FS
     */
    'click #saveProjectBtn': function() {
        // update current project's document
        var activeProject = Projects.findOne({ active: true }),
            projectTests = TestCases.find({ projectId: activeProject._id }),
        // collect project's data
        exportData = collectProjectData({
            project: activeProject,
            tests: projectTests
        }),
        // send an XMLHttpRequest for saving project
        saveProject = $.ajax({
            type: 'POST',
            url: activeProject.agentUrl + 'save',
            data: JSON.stringify(exportData),
            crossDomain: true,
            contentType: 'application/json',
            timeout: 5000
        });
        saveProject.done(function (resp) {
            if (resp.saved) {
                // add saved flag to project
                Meteor.call('updateProject', { _id: resp.projectId }, { $set: { savedAt: Date.now() }});
                toastr.success(activeProject.name + " saved!");
            } else {
                toastr.warning(activeProject.name + " save error (on server)");
            }
        });
        saveProject.fail(function (res, status, err) {
            if (res.status === 0)
                toastr.error("Check if agent runs", "Error sending request");
            else
                toastr.error(err.message);
        });
    },

    /**
     * Load project from agent, AFRET first shows all saved projects located on agent
     * then user can pick the right to load
     */
    'click #loadProjectBtn': function() {

        // TODO: modal window to choose which agent's project to load
        var agentModal = $('#agentModal'),
            agentList = $('#agentList');
            agents = _.uniq(Projects.find({}, {
                sort: { agentUrl: 1}, fields: { agentUrl: true}
            }).fetch().map(function(x) { return x.agentUrl }), true);

        for (var i = 0; i < agents.length; i++) {
            agentList.append(
                '<a href="#" class="list-group-item agentItem">' + agents[i] + '</a>'
            );
        }
        agentModal.modal('show');
        $('a.agentItem').click(function(evt) {
            var chosenAgent = evt.target.textContent;
            agentModal.modal('hide');
            agentList.empty();
            var getSavedProjects = $.ajax({
                url: chosenAgent + 'savedProjects',
                crossDomain: true,
                timeout: 3000
            });
            // on success
            getSavedProjects.done(function(resp) {
                var savedProjects = resp.savedProjects,
                    savedProjectList = $('#savedProjectList'),
                    loadProjectModal = $('#loadProjectModal');
                // get user's input
                for (var i = 0; i < savedProjects.length; i++) {
                    savedProjectList.append(
                        '<a href="#" class="list-group-item savedProject">' + savedProjects[i] + '</a>'
                    );
                }
                loadProjectModal.modal('show');
                $('a.savedProject').click(function(evt) {
                    var chosenProjectName = evt.target.textContent;
                    loadProjectModal.modal('hide');
                    savedProjectList.empty();
                    // ask for selected project
                    var loadProject = $.ajax({
                        url: chosenAgent + '/load',
                        data: { projectName: chosenProjectName },
                        crossDomain: true,
                        contentType: 'application/json',
                        timeout: 3000
                    });
                    loadProject.done(function(resp) {
                        toastr.success(chosenProjectName + " loaded!");
                        var loadObj = resp;
                        // make previous project inactive
                        Meteor.call('updateProject', { active: true }, { $set: { active: false } });
                        Meteor.call('importProject', loadObj, function(err, resp) {
                            if (err) { return console.log(err) }
                            if (!resp) {
                                toastr.error("File does not match DB schema", "Load error");
                            }
                        });
                    });
                    loadProject.fail(function(res, status, err) {
                        if (res.status === 0)
                            toastr.error("Check if agent runs", "Error sending request");
                        else
                            toastr.error(err.message);
                    });
                });
                $('#closeSavedProjectListBtn').click(function() {
                    savedProjectList.empty();
                });
            });
            // on failure
            getSavedProjects.fail(function(res, status, err) {
                if (res.status === 0)
                    toastr.error("Check if agent runs", "Error sending request");
                else
                    toastr.error(err.message);
            });
        });

    },

    /**
     * Switch to an inactive project
     */
    'click a.otherProject': function(evt) {
        // make previous project inactive
        Meteor.call('updateProject', { active: true }, { $set: { active: false } });
        // set new project active
        Meteor.call('updateProject', { _id: evt.target.getAttribute("id") }, { $set: { active: true } });
    }
});

/**
 * Serialize data
 */
function collectProjectData(projectObj) {
    var exportData = {};
    exportData.project = projectObj.project;
    exportData.tests = [];
    projectObj.tests.forEach(function (test) {
        exportData.tests.push(test)
    });
    return exportData;
}

/**
 * method returns datetime string of current time
 * used in export file name
 */
function getExportDate() {
    var d = new Date(),
        month = '' + d.getMonth() + 1,
        day = '' + d.getDate(),
        year = '' + d.getFullYear(),
        hour = '' + d.getHours(),
        min = '' + d.getMinutes(),
        sec = '' + d.getSeconds();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (hour.length < 2) hour = '0' + hour;
    if (min.length < 2) min = '0' + min;
    if (sec.length < 2) sec = '0' + sec;
    return [[year, month, day].join('-'), [hour, min, sec].join('-')].join('--');
}