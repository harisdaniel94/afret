/**
 * Created by danielharis on 09/04/2016.
 */

Template.report.helpers({

    'project': function() {
        var activeProject = Projects.findOne({ active: true });
        if (!activeProject) return;
        activeProject.totalTests = TestCases.find({ projectId: activeProject._id }).count();
        activeProject.passedTests = TestCases.find({
            projectId: activeProject._id,
            result: true,
            error: false
        }).count();
        activeProject.failedTests = activeProject.totalTests - activeProject.passedTests;
        activeProject.document = activeProject.document.replace(/style=".*?"/g, '');
        return activeProject;
    },
    'tests': function() {
        var activeProject = Projects.findOne({ active: true });
        return TestCases.find({ projectId: activeProject._id });
    }

});
