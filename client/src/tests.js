/**
 * Created by danielharis on 14/03/2016.
 */

Template.tests.helpers({

    'testCase': function() {
        var activeProject = Projects.findOne({ active: true});
        if (activeProject) {
            return TestCases.find({projectId: activeProject._id});
        }
    },
    'getSelected': function() {
        if (this._id == Session.get('selectedTest')) {
            return "selected";
        }
    },
    'activeProject': function() {
        return (Projects.findOne({ active: true })) ? true : false;
    },
    'projectTemplated': function() {
        var activeProject = Projects.findOne({ active: true });
        return activeProject && activeProject.templated;
    },
    'enableTemplateBtn': function() {
        var activeProject = Projects.findOne({ active: true });
        if (!activeProject)
            return false;
        return TestCases.find({ projectId: activeProject._id }).count() > 0;
    },
    'enableExecuteBtn': function() {
        return Projects.findOne({ active: true, templated: true }) ? true : false;
    }

});

Template.tests.events({

    /**
     * Make selected test case active
     */
    'click .tc': function() {
        // set previous selected test
        Session.set('previousTest', Session.get('selectedTest'));
        // set selected test
        Session.set('selectedTest', this._id);
        // if "selectedLines" list is non-empty, append lines to selected test
        if (Session.get('selectedLines').length) {
            // add all lines in list
            for (var i = 0; i < Session.get('selectedLines').length; i++) {
                Meteor.call('updateTest', { _id: Session.get('selectedTest') },
                    { $push: { lines: {
                        lineId: Session.get('selectedLines')[i].lineId,
                        content: Session.get('selectedLines')[i].content
                    }}}
                );
            }
            Session.set('selectedLines', []);
        }
        // delete previous and show current test's lines
        showNewLines();
    },

    /**
     * Create test case
     */
    'click #createTestBtn': function() {
        // can create test only if there's an active project open
        var activeProj = Projects.findOne({ active: true });
        if (!activeProj)
            return;
        // if project already templated, set templated flag to false
        if (activeProj.templated)
            Meteor.call('updateProject', { _id: activeProj._id }, { $set: { templated: false } });
        // set previous selected test
        Session.set('previousTest', Session.get('selectedTest'));
        var color = "#000000".replace(/0/g, function () {
            return (~~(Math.random() * 16)).toString(16);
        });
        // insert new test case and return it's ID
        Session.set('selectedTest', TestCases.insert({
            projectId: activeProj._id,
            name: "New Test",
            lines: [],
            color: color,
            executed: false,
            result: false,
            error: false
        }));
        // insert selected lines which has no test bound
        addLinesToCreatedTest();
        // delete previous and show current test's lines
        showNewLines();
    },

    /**
     * Remove test case
     */
    'click .removeBtn': function(evt) {

        // first remove all highlights of removed test
        var activeProject = Projects.findOne({ active: true }),
            testToBeRemovedId = $(evt.target.closest('tr')).attr('id'),
            testToBeRemovedLines = TestCases.findOne({_id: testToBeRemovedId}).lines;
        // if project already templated, set templated flag to false
        if (activeProject.templated)
            Meteor.call('updateProject', { _id: activeProject._id }, { $set: { templated: false } });
        // unwrap all highlighted lines
        for (var i = 0; i < testToBeRemovedLines.length; i++)
            $('span[lineId=' + testToBeRemovedLines[i].lineId + ']').contents().unwrap();
        // normalize document content after unwrap
        var doc = $('#doc');
        doc.each(function (index, text) { text.normalize() });
        // update document in DB
        Meteor.call('updateProject', { active: true }, { $set: { document: doc.html() } });
        // then delete test from DB
        Meteor.call('removeTest', { _id: testToBeRemovedId });
        // if equal - selectedTest='', previousTest=''
        if (testToBeRemovedId == Session.get('selectedTest')) {
            Session.set('selectedTest', '');
            Session.set('previousTest', '');
        // else - selectedTest=selectedTest, previousTest=''
        } else {
            Session.set('previousTest', '');
        }
        showNewLines();
    },

    /**
     * Generate a test template on agent
     */
    'click #templateBtn': function() {

        var activeProject = Projects.findOne({ active: true });
        if (!activeProject) return;
        var projectTests = TestCases.find({ projectId: activeProject._id }),
            testNames = [],
            nameDuplicate = false,
            invalidName = false;
        if (!projectTests.count()) return;
        // check test names
        projectTests.forEach(function(test) {
            // test name is invalid
            if (!test.name.match(/^[a-zA-Z_][a-zA-Z0-9_]*$/)) {
                invalidName = true;
                return;
            }
            // there is at least 2 tests with same name
            if ($.inArray(test.name, testNames) !== -1) {
                nameDuplicate = true;
                return;
            } else {
                testNames.push(test.name);
            }
        });
        if (nameDuplicate) {
            toastr.warning("All tests must have unique names!", "Test name duplicate");
            return;
        } else if (invalidName) {
            toastr.warning("Test name must match '^[a-zA-Z_][a-zA-Z0-9_]*$'", "Invalid test name");
            return;
        }
        // correct data

        // collect project data for template generation
        var projectObj = collectProjectData({
            project: {
                id: activeProject._id,
                name: activeProject.name,
                language: activeProject.language
            },
            tests: projectTests
        });
        // send an XMLHttpRequest for template creation
        var createTemplate = $.ajax({
            type: 'POST',
            url: activeProject.agentUrl + 'template',
            crossDomain: true,
            data: JSON.stringify(projectObj),
            contentType: 'application/json',
            timeout: 5000
        });
        createTemplate.done(function(resp) {
            toastr.success(activeProject.name + " templated!");
            // set templated flag and give it a timestamp
            Meteor.call('updateProject', { _id: resp.projectId }, { $set: {
                templated: resp.templated,
                templatedAt: new Date()
            }});
        });
        createTemplate.fail(function(res, status, err) {
            if (res.status === 0)
                toastr.error("Check if agent runs", "Error sending request");
            else
                toastr.error(err.message);
            // set templated to false
            Meteor.call('updateProject', { _id: activeProject._id }, { $set: { templated: false }});
        });
    },

    /**
     * Executes the test file located on agent in current project
     */
    'click #executeBtn': function () {

        var activeProject = Projects.findOne({ active: true });
        if (!activeProject || !activeProject.templated)
            return;
        // prepare data - only project name
        var testData = { projectName: activeProject.name };
        // send an XMLHttpRequest for template creation
        var executeTests = $.ajax({
            url: activeProject.agentUrl + 'execute',
            data: testData,
            crossDomain: true,
            contentType: 'application/json',
            timeout: 3000
        });
        executeTests.done(function(resp) {
            var status = resp.status,
                pName = resp.projectName,
                total = resp.totalTests;
            console.log(resp);
            if (status === "PASS") {
                Meteor.call('writeResults', pName, true);
                // add executedAt timestamp
                Meteor.call('updateProject', { _id: activeProject._id }, { $set: { executedAt: new Date() } });
                toastr.success("all passed (total of " + total + " tests)", pName + "'s test results");
            } else if (status === "FAIL") {
                var errors = resp.testErrors.length,
                    fails = resp.failedTests.length,
                    passes = total - errors - fails;
                Meteor.call('writeResults', pName, true, resp.failedTests, resp.testErrors);
                // add executedAt timestamp
                Meteor.call('updateProject', { _id: activeProject._id }, { $set: { executedAt: new Date() } });
                toastr.warning(
                    passes + " of " + total + " passed (" + fails + " fails, " + errors + " errors)",
                    pName + "'s test results"
                );
            } else { // if status === "E"
                Meteor.call('writeResults', pName, false);
                toastr.error("ERROR: " + resp.error, pName + "'s error");
            }
            var activeProjectsTests = TestCases.find({ projectId: activeProject._id }),
                resultsClasses = "tPass tFail tError";
            // visualise test results
            activeProjectsTests.forEach(function(test) {
                // PASS
                if (test.result && !test.error) {
                    for (var i = 0; i < test.lines.length; i++)
                        $('span.docLine[lineId=' + test.lines[i].lineId + ']').removeClass(resultsClasses).addClass('tPass');
                // FAIL
                } else if (!test.result && !test.error) {
                    for (var i = 0; i < test.lines.length; i++)
                        $('span.docLine[lineId=' + test.lines[i].lineId + ']').removeClass(resultsClasses).addClass('tFail');
                // ERROR
                } else {
                    for (var i = 0; i < test.lines.length; i++)
                        $('span.docLine[lineId=' + test.lines[i].lineId + ']').removeClass(resultsClasses).addClass('tError');
                }
            });
            // update document content
            var doc = $('#doc').html();
            $('#doc').empty();
            // update document in DB
            Meteor.call('updateProject', { active: true }, { $set: { document: doc } });
        });
        executeTests.fail(function(res, status, err) {
            if (res.status === 0)
                toastr.error("Check if agent runs", "Error sending request");
            else
                toastr.error(err.message);
        });
    }
});

/**
 * Function adds unlinked requirements if a test case is created
 */
function addLinesToCreatedTest() {

    if (Session.get('selectedLines') && Session.get('selectedLines').length) {
        for (var i = 0; i < Session.get('selectedLines').length; i++) {
            Meteor.call('updateTest', { _id: Session.get('selectedTest') },
                { $push: { lines: {
                    lineId: Session.get('selectedLines')[i].lineId,
                    content: Session.get('selectedLines')[i].content
                }}}
            );
        }
        Session.set('selectedLines', []);
    }

}

/**
 * Function shows new project's selected requirements
 */
function showNewLines() {

    if (Session.get('previousTest')) {
        var prevTest = TestCases.findOne({_id: Session.get('previousTest')});
        if (prevTest && prevTest.lines.length) {
            // first, delete previous test's lines
            for (var i = 0; i < prevTest.lines.length; i++) {
                $('span[lineId=' + prevTest.lines[i].lineId + ']').removeAttr("style");
            }
        }
    }
    if (Session.get('selectedTest')) {
        var currTest = TestCases.findOne({_id: Session.get('selectedTest')});
        if (currTest && currTest.lines.length) {
            // on click, apply test's color to his selected lines
            for (var j = 0; j < currTest.lines.length; j++) {
                $('span[lineId=' + currTest.lines[j].lineId + ']').attr(
                    "style", "background-color:" + currTest.color + ";"
                );
            }
        }
    }

}

/**
 * Serialize data for importing
 */
function collectProjectData(projectObj) {
    var exportData = {};
    exportData.project = projectObj.project;
    exportData.tests = [];
    projectObj.tests.forEach(function (test) {
        exportData.tests.push(test)
    });
    return exportData;
}