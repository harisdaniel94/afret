/**
 * Created by danielharis on 14/03/2016.
 */

/**
 * Create MongoDB collections
 */
Projects = new Mongo.Collection('projects');
TestCases = new Mongo.Collection('testCases');
var Schemas = {};

/**
 * Requirement schema
 */
Schemas.Line = new SimpleSchema({
    lineId: {
        type: String
    },
    content: {
        type: String
    }
});

/**
 * Test Case Schema
 */
Schemas.Test = new SimpleSchema({
    projectId: {
        type: String
    },
    name: {
        type: String
    },
    lines: {
        type: [Schemas.Line]
    },
    color: {
        type: String
    },
    executed: {
        type: Boolean
    },
    result: {
        type: Boolean
    },
    error: {
        type: Boolean
    }
});

/**
 * Project Schema
 */
Schemas.Project = new SimpleSchema({
    name: {
        type: String
    },
    language: {
        type: String
    },
    agentUrl: {
        type: String
    },
    document: {
        type: String
    },
    createdAt: {
        type: Date,
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            }
        }
    },
    templatedAt: {
      type: Date,
        optional: true
    },
    executedAt: {
        type: Date,
        optional: true
    },
    savedAt: {
        type: Date,
        optional: true
    },
    active: {
        type: Boolean,
        autoValue: function() {
            if (this.isInsert) {
                return true;
            }
        }
    },
    templated: {
        type: Boolean
    }
});

/**
 * Attach previously created schemas to collections
 */
Projects.attachSchema(Schemas.Project);
TestCases.attachSchema(Schemas.Test);
