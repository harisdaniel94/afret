/**
 * Created by danielharis on 14/03/2016.
 */

/**
 * Meteor methods
 * - possible to run client code on server
 */
Meteor.methods({

    'updateProject': function(selectObj, updateObj) {
        Projects.update(selectObj, updateObj);
    },
    'updateTest': function(selectObj, updateObj) {
        TestCases.update(selectObj, updateObj);
    },
    'removeProject': function(removeObj) {
        Projects.remove(removeObj);
    },
    'removeTest': function (removeObj) {
        TestCases.remove(removeObj);
    },
    'importProject': function(importObj) {
        if (!importObj.project) {
            return false;
        }
        var projId = importObj.project._id;
        if (Projects.findOne({ _id: projId })) {
            if (!Projects.findOne({ _id: projId }).active)
                Projects.update({ _id: projId }, { $set: { active: true } });
        } else {
            console.log(importObj);
            if (!projId)
                return false;
            delete importObj.project._id;
            importObj.project.createdAt = new Date(importObj.project.createdAt);
            if (importObj.project.savedAt)
                importObj.project.savedAt = new Date(importObj.project.savedAt);
            if (importObj.project.templatedAt)
                importObj.project.templatedAt = new Date(importObj.project.templatedAt);
            if (importObj.project.executedAt)
                importObj.project.executedAt = new Date(importObj.project.executedAt);
            try { check(importObj.project, Projects.simpleSchema()) } catch (e) {
                return false;
            }
            importObj.project._id = projId;
            Projects.insert(importObj.project);
            for (var i = 0; i < importObj.tests.length; i++) {
                var testId = importObj.tests[i]._id;
                delete importObj.tests[i]._id;
                try { check(importObj.tests[i], TestCases.simpleSchema()) } catch (e) {
                    return false;
                }
                importObj.tests[i]._id = testId;
                TestCases.insert(importObj.tests[i]);
            }
        }
        return true;
    },
    'writeResults': function(projectName, executed, fails, errors) {
        var projId = Projects.findOne({ name: projectName })._id;
        if (executed) {
            TestCases.update(
                { projectId: projId },
                { $set: { executed: true }},
                { multi: true }
            );
            if (fails || errors) { // if status FAIL
                var i,
                    testsWithErrors = [],
                    failedTests = [];
                if (fails) {
                    for (i = 0; i < fails.length; i++)
                        failedTests.push(fails[i].testName);
                    TestCases.update(
                        { $and: [
                            { projectId: projId },
                            { name: { $in: failedTests } }
                        ]},
                        { $set: { result: false, error: false } },
                        { multi: true }
                    );
                }
                if (errors) {
                    for (i = 0; i < errors.length; i++)
                        testsWithErrors.push(errors[i].testName);
                    TestCases.update(
                        { $and: [
                            { projectId: projId },
                            { name: { $in: testsWithErrors } }
                        ]},
                        { $set: { result: false, error: true } },
                        { multi: true }
                    );
                }
                var testsWhichNotPassed = failedTests.concat(testsWithErrors);
                // set result to true for all passed tests
                TestCases.update(
                    { $and: [
                        { name: { $nin: testsWhichNotPassed } },
                        { projectId: projId }
                    ]},
                    { $set: { result: true, error: false } },
                    { multi: true }
                );
            } else { // if status OK
                // set result to true for all tests
                TestCases.update(
                    { projectId: projId },
                    { $set: { result: true, error: false }},
                    { multi: true }
                );
            }
        } else { // if status ERROR
            // set error to true for all tests
            TestCases.update(
                { projectId: projId },
                { $set: { error: true } },
                { multi: true }
            );
        }
    }

});
