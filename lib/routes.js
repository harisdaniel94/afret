/**
 * Created by danielharis on 09/04/2016.
 */

/**
 * Define which template is rendered by which template
 */
Router.route('/', { template: 'main' });
Router.route('/report', { name: 'report' });
