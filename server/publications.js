/**
 * Created by danielharis on 14/03/2016.
 */

/**
 * Meteor's publish-subscribe mechanism
 * - here, I am publishing all Projects and Test Cases to client, but can be modified
 */
Meteor.publish('allTests', function() {
    return TestCases.find();
});

Meteor.publish('allProjects', function() {
    return Projects.find();
});
